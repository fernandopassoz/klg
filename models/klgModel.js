const mongoose = require('mongoose');

const User = mongoose.model('Klg', new mongoose.Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  site: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  strength:{
    type: String,
    required: true
  }
}),'klg')

module.exports = User