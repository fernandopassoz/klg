var express = require('express');
var router = express.Router();
const klg = require('../controllers/klgController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/klg/:site/:username',klg.find)

module.exports = router;
