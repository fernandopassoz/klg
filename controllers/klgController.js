require('../config/dbconfig');
const Klg = require('../models/klgModel');

const find = async(req, res, next) => {
  const klgs = await Klg.find({
    username: RegExp(req.params.username),
    site: RegExp(req.params.site)
  },null, {sort: {username: 1}})

  res.json(klgs)
}

module.exports = {
  find
}
